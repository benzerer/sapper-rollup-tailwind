import express from 'Express';
import sirv from 'sirv';
import { json } from 'body-parser';
import cors from 'cors';
import compression from 'compression';
import * as sapper from '@sapper/server';

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

const app = express()
	.use(
		cors(),
		json(),
		compression({ threshold: 0 }),
		sirv('static', { dev }),
		sapper.middleware()
	)

if ( dev ) {
	app.listen(PORT, err => {
		if (err) console.error('error', err);
	});
}

export { app };