module.exports = {
  purge: {
    mode : "all",
    enabled : true,
    content : ['./src/**/*.svelte'],
  },
  variants: {},
  theme: {
    extend: {}
  },
  plugins: [],
  future: {
      removeDeprecatedGapUtilities: true,
      purgeLayersByDefault: true,
  },
}
